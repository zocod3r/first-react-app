import React from 'react'

export default function Form() {
  return (
    <div className='forms'>
        <div className='form'>
            <div className="mb-3">
                <label for="exampleFormControlInput1" className="form-label">If you want to order our furniture or have any questions or inquiries, please contact us now. </label>
                <input type="email" className="form-control-input" id="exampleFormControlInput1" placeholder="Enter you full name"/>
                <input type="email" className="form-control-input" id="exampleFormControlInput1" placeholder="Enter you phone number"/>
                <input type="email" className="form-control-input" id="exampleFormControlInput1" placeholder="Enter you e-mail"/>
            </div>
            <div class="mb-3">
            <textarea className="form-control-textarea" id="exampleFormControlTextarea1" rows="3" placeholder='Type you message'></textarea>
            </div>
            <div class="d-grid gap-2">
                <button className="form-control-btn" type="button">Send</button>
            </div>
        </div>
    </div>
  )
}
