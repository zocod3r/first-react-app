import React from 'react'
import Cardslide from '../Cardslide'
import Footer from '../Footer'
import Navbar from '../Navbar'
import './catalog.css'
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import 'swiper/swiper-bundle.min.css'
import 'swiper/swiper.min.css'

// import required modules
import { Navigation } from "swiper";

export default function Catalogue() {
  return (
    <div className='Catalogue'>
      <Navbar/>
      <div className='Furniture'>
        <h1>Furniture</h1>
      </div>
      <h3 className='headBeddrom'>Bedroom</h3>
      <Cardslide/>
      <h3 className='headBeddrom'>Dining Room</h3>
      <>
    <Swiper
      slidesPerView={3}
      spaceBetween={30}
      slidesPerGroup={1}
      loop={true}
      loopFillGroupWithBlank={true}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[ Navigation]}
      className="mySwiper"
    >
      <SwiperSlide>
        <div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3>French Country Dining Set</h3>
                        <p>The Dining set is designed in traditional european style yet in more pratical shape</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </SwiperSlide>
      <SwiperSlide><div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3> Modern Shaker Dining Set</h3>
                        <p>The elegant look of this modern Dining set combines many decorating styles.</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div></SwiperSlide>
      <SwiperSlide><div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3>Classic Shaker Dining Set</h3>
                        <p>Classic Shaker Dining Set is created in traditional style and futured versatility</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div></SwiperSlide>
      <SwiperSlide><div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3>Naturel  Cherry Dining Set</h3>
                        <p>This set  is perfect balance of classic style and a bit contemporay flare</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </SwiperSlide>    
    </Swiper>
  </>
      <h3 className='headBeddrom'>Occasional</h3>
      <Cardslide/>
      <div className='contact'>
        <div className='info-contact'>
          <h3>Contacts</h3>
          <p>Feel free to get in touch with us for any question you may come across.</p>
          <div className='cont'>
          <div className='tit'>
            <h4>Address</h4>
            <h4>Hours</h4>
            <h4>Contacts</h4>
          </div>
          <div className='tex'>
            <p>Head office - 171 Tavistock Road, London</p>
            <p>Monday - Saturday: 11 AM - 9 PM Sunday: 11 AM - 7 PM</p>
            <p>+1 (234) 567 89 00 furi@a.draftium.com</p>
          </div>
          </div>
        </div>
        <div className='formu'>
        <div class="form-floating mb-3">
          <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com"/>
          <label for="floatingInput">Enter your full name</label>
        </div>
        <div class="form-floating">
          <input type="password" class="form-control" id="floatingInput" placeholder="Password"/>
          <label for="floatingPassword">Enter your phone number</label>
        </div>
        <div class="form-floating mb-3">
          <input type="email" className="form-control" id="floatingInput" placeholder="name@example.com"/>
          <label for="floatingInput">Enter your email</label>
        </div>
        <div class="form-floating">
          <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
          <label for="floatingTextarea">Type your message</label>
        </div>
        <button  className='quot' type="button" >Request a quote</button>
        </div>
      </div>
      <Footer/>
    </div>
  )
}
