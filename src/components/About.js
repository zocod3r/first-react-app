import React from 'react'

export default function 
() {
  return (
    <div className='about'>
        <div className='image-about'>
            <img className='imge' src='images/image.png'></img>
        </div>
        <h2 className="head-about">About Us</h2>
        <p className='p'>Furi is a leading furniture manufacturer known worldwide for its impeccable quality, exceptional customer service, and the most affordable prices. This is a family-owned, professional furniture company conveniently nestled in the heart of London. For more than 35 years, Furi has been building its own brand whilst also becoming the best licensing partner to such top lifestyle brands like Ralph Lauren, Perennials, and Kelly Wearstler. </p>
        <p className='p'>We take pride in our dedicated employees committed to design and build the best-in-class furniture for wholesale dealers at reasonable prices.</p>
        <button type='button' className='btnAbout'>Read more</button>
    </div>
  )
}
