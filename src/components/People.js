import React from 'react'
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

import 'swiper/swiper-bundle.min.css'
import 'swiper/swiper.min.css'
// Import Swiper styles


import "../people.css";

// import required modules
import { Pagination } from "swiper";
export default function People() {
  return (
    <>
    <Swiper
      slidesPerView={1}
      spaceBetween={30}
      pagination={{
        clickable: true,
      }}
      modules={[Pagination]}
      className="mySwiper"
    >
      <SwiperSlide>
        <div className='people'>
          <div className='image'>
            <img src='images/image.png'/>
          </div>
          <div className='temoignages'>
            <h2 className='head'>What People Say</h2>
            <hr></hr>
            <p>We have been looking for a dresser which will 
              fit our bedroom stylefor nearly 2 months. And
              finally, we have found it here, at Furi. This
              contemporary Craftsman 3-Drawer Dresser perfectly meets
              our needs and ompletes the look of our room. Thanks you
            </p>
            <h2>Kate Holmes</h2>
          </div>
        </div>
      </SwiperSlide>
      <SwiperSlide><div className='people'>
          <div className='image'>
            <img src='images/image.png'/>
          </div>
          <div className='temoignages'>
            <h2 className='head'>What People Say</h2>
            <hr></hr>
            <p>I was highly surprised wiht such smooth ordering
              process, unlike anywhere else. The delivery 
              of our Classic Shaker Dining Table was on time.
              Everybody is satisfied with the price and quality of 
              our new table. Thank a lot
            </p>
            <h2>Ann Robertson</h2>
          </div>
        </div></SwiperSlide>
      <SwiperSlide><div className='people'>
          <div className='image'>
            <img src='images/image.png'/>
          </div>
          <div className='temoignages'>
            <h2 className='head'>What People Say</h2>
            <hr></hr>
            <p>We have been looking for a dresser which will 
              fit our bedroom stylefor nearly 2 months. And
              finally, we have found it here, at Furi. This
              contemporary Craftsman 3-Drawer Dresser perfectly meets
              our needs and ompletes the look of our room. Thanks you
            </p>
            <h2>Jane Stone</h2>
          </div>
        </div></SwiperSlide>
    </Swiper>
  </>
  )
}
