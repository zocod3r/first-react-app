import React from 'react'

export default function Categorie() {
  return (
    <div className='category'>
        <h2 className='headCategorie' >Catégories</h2>
        <div className='categorie-grid'>
        <div className='categorie'>
              <div className='title'>
                <h3>Bedroom <br/>Furniture</h3>
              </div>
              <span className='desc'>We offer custom-made wooden bedroom furniture which adds more tranquility and comfort.</span>
        </div>
        <div className='categorie'>
              <div className='title'>
                <h3>Living Room <br/>Furniture</h3>
              </div>
              <span className='desc'>Handcrafted living room furniture made of wood will give your room more style and harmony.</span>
        </div>
        <div className='categorie'>
              <div className='title'>
                <h3>Home Office  <br/>Furniture</h3>
              </div>
              <span className='desc'>Well-organized home office workplace designed especially for you will make you more productive.</span>
        </div>
        </div> 
        <div className='button'>
            <button type='button' className='bnt-categorie'>All Catégories</button>
        </div>
    </div>
  )
}
