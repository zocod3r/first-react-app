import React from 'react'

export default function Maps() {
  return (
    <div className='Map'>
        <iframe className='map' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3858.448309453646!2d-17.4553734849846!3d14.743754489711387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xec10d43bf53eca9%3A0x3b82f22203a8a02d!2sVolkeno!5e0!3m2!1sfr!2ssn!4v1648208655884!5m2!1sfr!2ssn" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
        </iframe>
    </div>
  )
}
