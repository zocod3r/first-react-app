import React from 'react'
import { Link } from 'react-router-dom'
export default function Banner() {
  return (
    <div className='banner'>
        <h1 className='txtFuri'>Furi — Wholesale Furniture Production</h1>
        <p className='txtCreating'>Creating affordable and functional furniture for your home</p>
        <button className='btnAbou'><Link className='links' to='/about'>About Us</Link></button>
    </div>
  )
}
