import React from 'react'
import Form  from '../Form'
import Footer from '../Footer'
import Navbar from '../Navbar'
import './contact.css'
import { Link } from 'react-router-dom'
export default function Contact() {
  return (
    <div className='contac'>
      <Navbar/>
      <h1 className='Contact'>Contact</h1>
      <div className='Contacts'>
          <div className='contact'>
          <div className='colum'>
          <iframe className='map' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3858.448309453646!2d-17.4553734849846!3d14.743754489711387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xec10d43bf53eca9%3A0x3b82f22203a8a02d!2sVolkeno!5e0!3m2!1sfr!2ssn!4v1648210492830!5m2!1sfr!2ssn"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
          </iframe>
          </div>
          <div className='colums'>
          <h3 className='logo'><Link className='links' to='/'>company logo</Link></h3>
          <h3>Address</h3>
          <p>Head office - 171 Tavistock Road, London</p>
          <h3>Phone</h3>
          <a className='links' href='#'>+1 (234) 567 89 00</a>
          <h3>E-mail</h3>
          <div className='social-links'>
          <img src='images/facebook.png' className='social-link'>
          
          </img>
          <img src='images/instagram.png' className='social-link'>
          
          </img>
          <img src='images/youtube.png' className='social-link'>
          
          </img> 
        </div>
          </div>
        </div>
        </div>
    <Form/>
    <Footer/>
    </div>
  )
}
