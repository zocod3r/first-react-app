import React from 'react'
import Navbar from '../Navbar'
import  './project.css';
import  Footer from'../Footer';

export default function Project() {
  return (
    <div className='Project'>
        <Navbar/>
        <h1 className='head-project'>Our Projects</h1>
        <div className='Our-Work'>
            <h2>Our Work</h2>
            <p>At Furi, we provide wholesale clients with a wide range of furniture items for any room online and directly from our warehouse.</p>
            <p>Our furniture is created by real craftsmen and famous designers with customers' needs in mind. All the items perfectly combine style, functionality, comfort, and the highest quality possible. Look through the latest design projects and see the difference!</p>
        </div>
        <div className='BENCHES'>
          <div className='item-BENCHES'>
            <div className='item-imge'>
                <img src='images/image.png'/>
            </div>
            <div className='txt'>
            <span>BENCHES</span>
            <h2>Cherry Bench with Leather Seat</h2>
            <p>This solid cherry wood bench comes with a leather seat in white, black, and brown colors. The bench features American style and modern design.</p>
            <a className='links' href='#'>Learn more</a>
            </div>
          </div>
        </div>
        <div className='DESKS'>
          <div className='item-DESKS'>
            <span>DESKS</span>
            <h2>Modern Writing Desk</h2>
            <p>The high-quality modern writing desk is designed to impress anybody who visits your home office. Everything you need is organized at your fingertips.</p>
            <a className='links' href='#'>Learn more</a>
          </div>
          <div className='DESKS-img'>
            <img src='images/image.png'/>
          </div>
        </div>
        <div className='SOFAS'>
          <div className='item-BENCHES'>
            <div className='item-imge'>
                <img src='images/image.png'/>
            </div>
            <div className='txt'>
            <span>SOFAS</span>
            <h2>Modern Sofa</h2>
            <p>We create the exceptional designs of contemporary sofas which complete the style of your living room and reflect your own lifestyle. Choose the one that suits you.</p>
            <a className='links' href='#'>Learn more</a>
            </div>
          </div>
        </div>
        <Footer/>
    </div>
   
  )
}
