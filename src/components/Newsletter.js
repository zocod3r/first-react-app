import React from 'react'

export default function 
() {
  return (
    <div className='news'>
        <p className='txt-news'>
            Subscribe to Our Newsletter <br/>
            and receive a 7% discount on the wholesale furniture order!
        </p>
        <div className="input-group mb-3">
            <input  type="text" class="form-control" placeholder="Email" aria-label="Email" />
            <button className='btn' class="btn btn-secondary" type="button" id="button-addon2">Subscribe</button>
        </div>
    </div>
  )
}
