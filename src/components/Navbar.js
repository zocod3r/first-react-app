import React, {useState, useEffect} from 'react'
import { Link } from 'react-router-dom'
import "../style.css"
export default function navbar() {
  const [toggleMenu, setToggleMenu]=useState(false);
  const [largeur, setLargeur]=useState(false);
  const toggleNavSmallScreen = () =>{
    setToggleMenu(!toggleMenu);
  }
  useEffect(() =>{
    const changeWidth=()=>{
      setLargeur(window.innerWidth); 
      if(window.innerWidth>400){
        setToggleMenu(false); 
      }
    }
   window.addEventListener('resize',changeWidth);
   return()=>{
     window.removeEventListener('resize', changeWidth);
   }
  },[])
  return (
    <nav>
        <h1 className='logo'><Link className='links' to='/'>Furi</Link></h1>
        {toggleMenu ? toggleMenu: 
          <ul className='menu'>
            <li className='items'><Link className='links' to='/about'>About Us</Link></li>
            <li className='items'><Link className='links' to='/catalog'>Catalog</Link></li>
            <li className='items'><Link className='links' to='/project'>Projects</Link></li>
            <li className='items'><Link className='links' to='/blog'>Blog</Link></li>
            <li className='items'><Link className='links' to='/contact'>Contacts</Link> </li>
          </ul>
        }
        <button onClick={toggleNavSmallScreen} className='btn' type='button'>Menu</button>
    </nav>
  )
}
 