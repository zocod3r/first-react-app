import React from 'react'
import { Link } from 'react-router-dom'
export default function Footer() {
  return (
    <div className='footer'>
      <div className='tag'>
        <h2> Furi</h2>
        <p>Furi is a leading furniture manufacturer known worldwide for its impeccable quality, exceptional customer service, and fair prices. </p>
      <p>Created with <a href='/' className='links'>Draftium</a> ©</p>
      </div>
      <div className='tag'>
        <p><Link className='links' to='/catalog'>Catalog</Link> </p>
        <p><a href='/' className='links'>Testimonials</a> </p>
        <p><Link className='links' to='/contact'>Contacts</Link> </p>  
      </div>
      <div className='tag'>
        <p><a href='/' className='links'>About Us</a> </p>
        <p><a href='/' className='links'>FAQ</a> </p> 
      </div>
      <div className='tag'>
        <p><Link className='links' to='/blog'>Blog</Link> </p>
        <p><Link className='links' to='/project'>Projects</Link></p> 
      </div>
      <div className='tag'>
        <h2>Follow US</h2>
        <div className='social-links'>
          <img src='images/facebook.png' className='social-link'>
          
          </img>
          <img src='images/instagram.png' className='social-link'>
          
          </img>
          <img src='images/youtube.png' className='social-link'>
          
          </img>
          <img src='images/pinterest.png' className='social-link'>
          
          </img>  
          <p className='all'> All Rights Reserved</p>
        </div>
      </div>
    </div>
  )
}
