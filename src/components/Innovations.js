import React from 'react'

export default function Innovations() {
  return (
    <div className='innovations'>
      <h2>Passion for Innovations</h2> 
      <p>The extensive experience of our talented craftsmen helps them create products with more capabilities, functionality, <br/> and safety</p>
      <div className='categorie-grid'>
        <div className='categorie'>
              <div className='title'>
                <img src='images/image.png'/>
              </div>
              <h3>Safety </h3>
              <span className='desc'>We can ensure our clients that all our furniture products are safe for the health of all their home inhabitants.</span>
        </div>
        <div className='categorie'>
              <div className='title'>
              <img src='images/image.png'/>
              </div>
              <h3>Functionality</h3>
              <span className='desc'>Be it a strengthened support structure or hidden support legs for futons, our craftsmen strive to push the boundaries in design functionality.</span>
        </div>
        <div className='categorie'>
              <div className='title'>
              <img src='images/image.png'/>
              </div>
              <h3>Design</h3>
              <span className='desc'>Our market is overfilled with unique furniture designs. Our goal is to stand out from the crowd and continue to impress our clients.</span>
        </div>
        </div> 
    </div>
  )
}
