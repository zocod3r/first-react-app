import React from 'react'
import './blog.css'
import Navbar from '../Navbar'
import Footer from '../Footer'
export default function Blog() {
  return (
    <div className='Blog'>
      <Navbar/>
      <h1 className='head-Blog'>Blog</h1>
      <div className='BENCHES'>
          <div className='item-BENCHES'>
            <div className='item-imge'>
                <img src='images/image.png'/>
            </div>
            <div className='txt'>
            <span>September 28, 2018|Furniture Care</span>
            <h2>Easy Care Tips to Keep Your Furniture Looking New</h2>
            <p>Once you have purchased the ideal item of furniture and your home design has been completed, you think you can relax and enjoy it. Follow some useful advice to enjoy it for as long as you can.</p>
            <a className='links' href='#'>Learn more</a>
            </div>
          </div>
        </div>
        <div className='latest-news'>
      <div className='categorie-grid'>
        <div className='categoriee'>
              <div className='title'>
                <img src='images/image.png'/>
              </div>
              <span>Post by Furi Team|September 22, 2018</span>
              <h4>Outdoor Furniture Items for this Summer </h4>
              <span className='desc'>Explore your local thrift store and you just might find the unexpected; a polka dotted bowl, a fluffy plant, a faux typewriter or camera, some books..</span><br/>
              <a className='links' href='#'>Learn more</a>
        </div>
        <div className='categoriee'>
              <div className='title'>
              <img src='images/image.png'/>
              </div>
              <span>Post by Furi Team|September 18, 2018</span>
              <h4>New Furniture Collection Release 2018/2019</h4>
              <span className='desc'>Explore this new collection which is introduced in the new finish, handmade of the finest wood that will add a touch of nature and at the same time luxury to your...</span><br/>
              <a className='links' href='#'>Learn more</a>
        </div>
        <div className='categoriee'>
              <div className='title'>
              <img src='images/image.png'/>
              </div>
              <span>Post by Furi Team|September 11, 2018</span>
              <h4>What's New? Leather Hickory Chair Sofas</h4>
              <span className='desc'>We are happy to offer our brand new product, Hickory chair sofa. It differs from other sofas by the material. We have used leather which looks more elegant, has a soft texture and...</span> <br/>
              <a className='links' href='#'>Learn more</a>
        </div>
        </div>
    </div>
        <Footer/>
    </div>
  )
}
