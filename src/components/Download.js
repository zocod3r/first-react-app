import React from 'react'

export default function 
() {
  return (
    <div className='Download'>
        <h2 className='down'>Download Furniture Catalog</h2>
        <button type='button' className='load'> <img className='charge' src='images/direct-download.png'/> Download PDF</button>
    </div>
  )
}
