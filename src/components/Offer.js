import React from 'react'

export default function 
() {
  return (
    <div className='Offer'>
        <h2>What We Offer</h2>
        <p className='pp'>Furi offers the highest quality wholesale furniture for real estate developers, furniture stores, interior designers, and online retailers. We provide worldwide access to our furniture with such advantages as:</p>
        <div className='offer'>
            <div className='card'>
                <img className='img' src='images/website.png'></img>
                <p>Furi Furniture Company Information</p>
            </div>
            <div className='card'>
            <img className='img' src='images/1000_F_465955205_BXnne73yhWqcrrrpLaBbsvRIHg6gZEMz.jpg'></img>
            <p>A wide range of mid-level to high-end furnishings</p>
            </div>
            <div className='card'>
            <img className='img' src='images/checklist.png'></img>
            <p> No minimum warehouse order requirements</p>
            </div>
            <div className='card'>
            <img className='img' src='images/website.png'></img>
            <p>Constantly updated catalog of products we offer</p>
            </div>
            <div className='card'>
            <img className='img' src='images/support.png'></img>
            <p>Impeccable customer service and smooth ordering process</p>
            </div>
            <div className='card'>
            <img className='img' src='images/maps.png'></img>
            <p>Worldwide container programs mixed from Italy and Asia</p>
            </div>
            <div className='card'>
            <img className='img' src='images/1000_F_112648256_WfRCC46q7OYDOmJDxUYfMpA2ybl6w3cI.jpg'></img>
            <p>Round the clock client support service</p>
            </div>
            <div className='card'>
            <img className='img' src='images/box.png'></img>
            <p>In-stock items ready for delivery</p>
            </div>
        </div>
    </div>
  )
}
