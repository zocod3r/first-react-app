import React from 'react'
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation } from 'swiper';
import 'swiper/swiper-bundle.min.css'
import 'swiper/swiper.min.css'
// Import Swiper styles


export default function Cardslide() {
  return (
    <>
    <Swiper
      slidesPerView={3}
      spaceBetween={30}
      slidesPerGroup={1}
      loop={true}
      loopFillGroupWithBlank={true}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[ Navigation]}
      className="mySwiper"
    >
      <SwiperSlide>
        <div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3>Cherry Moon Bed</h3>
                        <p>THis Bed is a handmake work designed for utmost comfort</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </SwiperSlide>
      <SwiperSlide><div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3>Indy Storage Bed</h3>
                        <p>Indy Storage Bed is able to improve any bedroom in the apartment of any size</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div></SwiperSlide>
      <SwiperSlide><div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3>Bold Storage Bed</h3>
                        <p>THis Bed is a great  combination of style, fonctionnaly, and comfort</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div></SwiperSlide>
      <SwiperSlide><div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3>Cherry Moon Bed</h3>
                        <p>THis Bed isa handmake work designed for utmost comfort</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div></SwiperSlide>
      <SwiperSlide><div className='Cardslide'>
        <div className='content'>
            <div className='cardslide'>
                <div className='card-content'>
                    <div className='image-card'>
                        <img src=' images/image.png' />
                    </div>
                    <div className='describe'>
                        <h3>Furi Shaker Moon Bed</h3>
                        <p>A shaker-style reliable bed designed to last for a lifetime</p>
                        <a className='links' href='#'>Learn more</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </SwiperSlide>
      
    </Swiper>
  </>
  )
}
