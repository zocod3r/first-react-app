import React from 'react'
import './about.css'
import Navbar from '../Navbar'
import Footer from '../Footer'
import Latest from '../Latest'
import People from '../People'
import { Link } from 'react-router-dom'
import Maps from '../Maps'

export default function Apropos() {
  return (
    <div className='about-us'>
        <Navbar/>
        <h1 className='aboutus'>About Us</h1>
        <div className='Shortly-About-Us'>
          <div className='item-text'>
            <h2>Shortly About Us</h2>
            <p>We are a leading furniture manufacturer known worldwide for the highest quality and exceptional customer service. We value our dedicated employees committed to design and build the best-in-class furniture for wholesale dealers at reasonable prices.</p>
          </div>
          <div className='item-img'>
            <img src='images/image.png'/>
          </div>
        </div>
        <div className='Partners'>
        <h2 className='headPartners'>Partners</h2>
        <div className='partners'>
          <div className='item'>
            <img className='im' src='images/image.png'/>
          </div> 
          <div className='item'>
            <img className='im' src='images/image.png'/>
          </div> 
          <div className='item'>
            <img className='im' src='images/image.png'/>
          </div> 
          <div className='item'>
            <img className='im' src='images/image.png'/>
          </div> 
          <div className='item'>
            <img className='im' src='images/image.png'/>
          </div> 
          <div className='item'>
            <img className='im' src='images/image.png'/>
          </div> 
          <div className='item'>
            <img className='im' src='images/image.png'/>
          </div> 
          <div className='item'>
            <img className='im' src='images/image.png'/>
          </div> 
          </div> 
        </div>
        <Latest/>
        <People/>
        <div className='FAQ'>
          <h2 className='headFaq'>FAQ</h2>
          <div className='faq'>
          <div className='grid'>
            <h3>Do you use family-friendly fabrics?</h3>
            <p>Yes! We use only family-friendly fabrics. Additionally, these fabrics have a function of repelling of any spills and stains which may occur during the daily usage.</p>
          </div>
          <div className='grid'>
            <h3>When can I expect my order?</h3>
            <p>Any furniture products available for purchase can be delivered after your request. Special orders will arrive at your location in nearly 6 - 8 weeks after the moment you pay for your order. You can also track your order status.</p>
          </div>
          <div className='grid'>
            <h3>Can I order an individual design of furniture item?</h3>
            <p>Yes. We also provide custom furniture manufacturing service. Contact us to discuss what item you need, your ideas, design, fabric, and measurements. </p>
          </div>
          <div className='grid'>
            <h3>What wood species do you use?</h3>
            <p>We manufacture all furniture products from natural wood. We currently use such wood types like cherry, pine tree, oak, maple, and other hardwoods.</p>
          </div>
          <div className='grid'>
            <h3>Can furniture be dissembled to move?</h3>
            <p>It depends on the type of furniture. Some products can be disassembled to facilitate transportation and movement; others cannot. If you have any questions, feel free to get in touch with us and specify all the details.</p>
          </div>
          <div className='grid'>
            <h3>Do you offer any guarantee?</h3>
            <p>Yes. We have more than 25 years of experience in the industry and proudly provide our clients with a 1-year guarantee against manufacturing defects on all our furniture items.</p>
          </div>
        </div>
        </div>
        <div className='Contacts'>
          <h2 className='headContacts'>Contacts</h2>
          <div className='contact'>
          <div className='colum'>
          <iframe className='map' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3858.448309453646!2d-17.4553734849846!3d14.743754489711387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xec10d43bf53eca9%3A0x3b82f22203a8a02d!2sVolkeno!5e0!3m2!1sfr!2ssn!4v1648210492830!5m2!1sfr!2ssn"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
          </iframe>
          </div>
          <div className='colums'>
          <h3 className='logo'><Link className='links' to='/'>company logo</Link></h3>
          <h3>Address</h3>
          <p>Head office - 171 Tavistock Road, London</p>
          <h3>Phone</h3>
          <a className='links' href='#'>+1 (234) 567 89 00</a>
          <h3>E-mail</h3>
          <div className='social-links'>
          <img src='images/facebook.png' className='social-link'>
          
          </img>
          <img src='images/instagram.png' className='social-link'>
          
          </img>
          <img src='images/youtube.png' className='social-link'>
          
          </img> 
        </div>
          </div>
        </div>
        </div>
        <Footer/>
    </div>
  )
}
