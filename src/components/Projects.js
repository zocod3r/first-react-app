import React from 'react'
export default function Projects() {
  return (
    <>
      <article className="Titres" for="">
          <h2>Our Projects</h2>
        </article>
        <header className="b">
          <div
            id="carouselExampleControls"
            className="carousel slide"
            data-bs-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img
                  src="images/image.png"
                  className="ims"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src="images/image.png"
                  className="ims"
                  alt="..."
                />
              </div>
              <div className="carousel-item">
                <img
                  src="images/image.png"
                  className="ims"
                  alt="..."
                />
              </div>
            </div>
            <button
              className="carousel-control-prev"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Previous</span>
            </button>
            <button
              className="carousel-control-next"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Next</span>
            </button>
          </div>
        </header>
<div className='button'>
  <button type='button' className='bnt-categorie'>All Projects</button>
</div>
  </>
  )
}
