import React from 'react'

export default function Latest() {
  return (
    <div className='latest-news'>
      <h2>Latest-news</h2>
      <div className='categorie-grid'>
        <div className='categoriee'>
              <div className='title'>
                <img src='images/image.png'/>
              </div>
              <span>Post by Furi Team|September 22, 2018</span>
              <h4>Outdoor Furniture Items for this Summer </h4>
              <span className='desc'>Explore your local thrift store and you just might find the unexpected; a polka dotted bowl, a fluffy plant, a faux typewriter or camera, some books..</span><br/>
              <a className='links' href='#'>Learn more</a>
        </div>
        <div className='categoriee'>
              <div className='title'>
              <img src='images/image.png'/>
              </div>
              <span>Post by Furi Team|September 18, 2018</span>
              <h4>New Furniture Collection Release 2018/2019</h4>
              <span className='desc'>Explore this new collection which is introduced in the new finish, handmade of the finest wood that will add a touch of nature and at the same time luxury to your...</span><br/>
              <a className='links' href='#'>Learn more</a>
        </div>
        <div className='categoriee'>
              <div className='title'>
              <img src='images/image.png'/>
              </div>
              <span>Post by Furi Team|September 11, 2018</span>
              <h4>What's New? Leather Hickory Chair Sofas</h4>
              <span className='desc'>We are happy to offer our brand new product, Hickory chair sofa. It differs from other sofas by the material. We have used leather which looks more elegant, has a soft texture and...</span> <br/>
              <a className='links' href='#'>Learn more</a>
        </div>
        </div>
        <div className='button'>
            <button type='button' className='bnt-latest'>All news</button>
        </div>
    </div>
  )
}
