import React from 'react'

export default function Products() {
  return (
    <div className='products'>
        <h2 className='headProd'>Featured Products</h2>
        <div className='produits'>
          <div className='produit'>
            <div className='produit#1'>
              <div className='image'>
                <img className='im' src='images/image.png'/>
              </div>
              <span className='article-name'>Cherry Moon Coffee</span>
              <p>Table - $1859</p>
              <a className='links' href='#'>Learn more</a>
            </div>
          </div>
          <div className='produit'>
            <div className='produit#2'>
            <div className='image'>
            <img className='im' src='images/image.png'/>
              </div>
              <span className='article-name'>Cherry Moon Coffee</span>
              <p>Table - $1859</p>
              <a className='links' href='#'>Learn more</a>
            </div>
          </div>
          <div className='produit'>
            <div className='produit#3'>
            <div className='image'>
            <img className='im' src='images/image.png'/>
              </div>
              <span className='article-name'>Cherry Moon Coffee</span>
              <p>Table - $1859</p>
              <a className='links' href='#'>Learn more</a>
            </div>
          </div>
          <div className='produit'>
            <div className='produit#4'>
            <div className='image'>
            <img className='im' src='images/image.png'/>
              </div>
              <span className='article-name'>Cherry Moon Coffee</span>
              <p>Table - $1859</p>
              <a className='links' href='#'>Learn more</a>
            </div>
          </div>
          <div className='produit'>
            <div className='produit#5'>
            <div className='image'>
            <img className='im' src='images/image.png'/>
              </div>
              <span className='article-name'>Cherry Moon Coffee</span>
              <p>Table - $1859</p>
              <a className='links' href='#'>Learn more</a>
            </div>
          </div>
          <div className='produit'>
            <div className='produit#6'>
            <div className='image'>
            <img className='im' src='images/image.png'/>
              </div>
              <span className='article-name'>Cherry Moon Coffee</span>
              <p>Table - $1859</p>
              <a className='links' href='#'>Learn more</a>
            </div>
          </div>
          <div className='produit'>
            <div className='produit#7'>
            <div className='image'>
            <img className='im' src='images/image.png'/>
              </div>
              <span className='article-name'>Cherry Moon Coffee</span>
              <p>Table - $1859</p>
              <a className='links' href='#'>Learn more</a>
            </div>
          </div>
          <div className='produit'>
            <div className='produit#8'>
            <div className='image'>
            <img className='im' src='images/image.png'/>
              </div>
              <span className='article-name'>Cherry Moon Coffee</span>
              <p>Table - $1859</p>
              <a className='links' href='#'>Learn more</a>
            </div>
          </div>
        </div>
    </div>
  )
}
