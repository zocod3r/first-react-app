import React from 'react';
import  './style.css';
import Navbar from './components/Navbar';
import Banner from './components/Banner';
import Products from './components/Products';
import Categories from './components/Categorie';
import Production from './components/Production';
import Projects from './components/Projects';
import Download from './components/Download';
import About from './components/About';
import Offer from './components/Offer';
import Newsletter from './components/Newsletter';
import Innovations from './components/Innovations';
import People from './components/People';
import Latest from './components/Latest';
import Form from './components/Form';
import Footer from './components/Footer';

export default function Home() {
  return (
    <div className='home'>
        <Navbar/>
        <Banner/>
        <Products/>
        <Categories/>
        <Production/>
        <Projects/>
        <Download/>
        <About/>
        <Offer/>
        <Newsletter/>
        <Innovations/>
        <People/>
        <Latest/>
        <Form/>
        <Footer/>
    </div>
  )
}
