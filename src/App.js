import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from 'react-router-dom'; //permet de naviguer entre les pages 
import './App.css';
import About from './components/About';
import Banner from './components/Banner';
import Categorie from './components/Categorie';
import Download from './components/Download';
import Footer from './components/Footer';
import Form from './components/Form';
import Innovations from './components/Innovations';
import Latest from './components/Latest';
import Navbar from './components/Navbar';
import Newsletter from './components/Newsletter';
import Offer from './components/Offer';
import People from './components/People';
import Production from './components/Production';
import Products from './components/Products';
import Projects from './components/Projects';
function App() { 
  let navigate = useNavigate();/*<nav>
  <Link className='links' to='/about'>About Us</Link>
  <Link className='links' to='/catalog'>Catalog</Link>
  <button onClick={()=>{navigate("/about")}}>To About</button>
  </nav>
  <Routes>
  <Route path='/about' element={<Apropos/>} />
  <Route path='/catalog' element={<Catalogue/>} />
  </Routes> */  
  return (
    
    <div className="App">        
      <Navbar />
      <Banner/>
      <Products/>
      <Categorie/>
      <Production/>
      <Projects/>
      <Download/>
      <About/>
      <Offer/>
      <Newsletter/>
      <Innovations/>
      <People/>
      <Latest/>
      <Form/>
      <Footer/>
    </div>
  );
}

export default App;
